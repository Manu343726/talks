#include "cpplib.h"
#include <QTimerEvent>
#include <chrono>
#include <iostream>

using namespace cpplib;

Class::Class() :
    _timerId{QObject::startTimer(std::chrono::milliseconds(1000).count())}
{}

void Class::value(int value)
{
    if(value != _value)
    {
        _value = value;
        emit valueChanged();
    }
}

int Class::value() const
{
    return _value;
}

void Class::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == _timerId)
    {
        std::cout << "tick\n";
    }
}
