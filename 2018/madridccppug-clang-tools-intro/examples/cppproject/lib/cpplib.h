#ifndef CPPPROJECT_LIB_CPPLIB_H
#define CPPPROJECT_LIB_CPPLIB_H

#include <QObject>

namespace cpplib
{

class Class : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int value READ value WRITE value);
public:
    Class();
    ~Class(){}

    void value(int value);
    int value() const;

signals:
    void valueChanged();

private:
    int _value = 0;
    int _timerId = 0;

    void timerEvent(QTimerEvent* event);
};

}

#endif // CPPPROJECT_LIB_CPPLIB_H
