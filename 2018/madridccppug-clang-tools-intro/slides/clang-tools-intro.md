<!-- $theme: default -->
<!-- foother: @Manu343726 - Manu343726@gmail.com - "Introduccion a clang tools" - Madrid C/C++ Meetup 26/07/18-->

Introduccion a clang tools
==========================

###### Manu Sanchez @Manu343726

---

<!-- page_number: true -->

### Introduccion a clang-tools
 - clang-format
 - clang-tidy
 - clang-check

 - include-what-you-use
 - clazy
 - cppinsights
 - woboq-codebrowser

 - Clang API
 - libclang
 - cppast

---

Primero: Que es Clang?
======================

---

### Que es Clang?

 - Un frontend C/C++ para LLVM
 - Open source
 - Organizado como una serie de APIs

---

## Compilation database

 - `compile_commands.json`
 - Conjunto de ordenes de compilacion

 ```
 [
 {
    "directory": "/home/manuel/projects/foo/build/libfoo"
    "command": "/usr/bin/c++ foo.cpp -o foo.o -DLIBFOO=1 -std=c++11 -I/home/manuel/projects/foo/include/"
    "file": "/home/manuel/projects/foo/libfoo/foo.cpp"
 },
 ...
 ]
 ```

 - Mas sencillo que introducir las opciones de compilacion a mano


---

## Compilation database: Generacion

 - CMake: `cmake .. -DCMAKE_EXPORT_COMPILE_COMMANDS=ON`
 - [Bear](https://github.com/rizsotto/Bear): `bear make all`

---

clang-format
============

---

 - Formateo de codigo segun guia de estilo
 - Incluye varias guias de estilo predeterminadas (Google, LLVM, etc)
 - Completamente configurable
 - Permite edicion automatica

---

clang-format demo
=================

---

 - [clang-format-configurator](https://zed0.co.uk/clang-format-configurator/)
 - pre-commit hook
 - Integracion en IDEs y editores de codigo

---

clang-tidy
==========

---

### clang-tidy

 - Analisis estatico + refactorizacion
 - Diferentes checks para diferentes objetivos
 - Permite refactorizacion automatica

---

clang-tidy
==========

 - Listar checks disponibles: `clang-tidy --list-checks -checks='*'`
 - `modernize`: Checks dedicados a migrar C++ legacy a C++11
 - `cppcoreguidelines`
 - `readability`

---

clang-tidy demo
===============

---

clang-checker
=============

---

 - Analisis estatico
 - Muy util para ver el AST

---

Herramientas basadas en clang
=============================

---

### [Include What You Use (IWYU)](https://include-what-you-use.org/)

 - Averigua que `#include`s necesita un fichero, y cuales no son necesarios
 - Instalacion en debian: `apt install iwyu`

---

### [Woboq Codebrowser](https://github.com/woboq/woboq_codebrowser)

 - Genera una pagina HTNL estatica indexando un proyecto C++ completo
 - Vinculos entre clases, funciones, ficheros, variables
 - Muestra documentacion inline

---

### [Clazy](https://github.com/KDE/clazy)

 - Analisis estatico para projectos basados en Qt
 - Genera warnings sobre malos usos y antipatrones de la API Qt

---

Clang API
=========

---

### Clang API

 - API C++
 - Basada en el patron visitor
 - Visitor + Matchers + Actions
 - Plugings o herramientas independientes

---

### Clang API: FrontendAction

 - Representa una interfaz para ejecutar acciones como parte de la compilacion
 - Es el punto de entrada de cualquier herramienta o plugin

``` cpp
class FindNamedClassAction : public clang::ASTFrontendAction {
public:
  virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
    clang::CompilerInstance &Compiler,
    llvm::StringRef InFile
  ) {
    return std::unique_ptr<clang::ASTConsumer>(
        new FindNamedClassConsumer);
  }
};
 ```

---

### Clang API: AstConsumer

 - Interfaz de entrada al arbol sintactico (AST, *"Abstract Syntax Tree"*).
 - Lanza la accion sobre un AST concreto

``` cpp
class FindNamedClassConsumer : public clang::ASTConsumer {
public:
  virtual void HandleTranslationUnit(clang::ASTContext &Context) {
    // Traversing the translation unit decl via a RecursiveASTVisitor
    // will visit all nodes in the AST.
    Visitor.TraverseDecl(Context.getTranslationUnitDecl());
  }
private:
  // A RecursiveASTVisitor implementation.
  FindNamedClassVisitor Visitor;
};
```

---

### Clang API: RecursiveAstVisitor

 - Visitor de acceso al AST
 - Provee metodos virtuales a modo de callbacks, uno por cada tipo de nodo
   (Declaracion de funcion, definicion de function, declaracion de variable, etc)

``` cpp
class FindNamedClassVisitor
  : public RecursiveASTVisitor<FindNamedClassVisitor> {
public:
  bool VisitCXXRecordDecl(CXXRecordDecl *Declaration) {
    // For debugging, dumping the AST nodes will show which
    // nodes are already being visited.
    Declaration->dump();

    // The return value indicates whether we want the visitation to proceed.
    // Return false to stop the traversal of the AST.
    return true;
  }
};
```
---

### Clang API

 - Ejemplo completo [En la documentation oficial](https://clang.llvm.org/docs/RAVFrontendAction.html)

---

### Clang API: Plugins

 - Como una herramienta, pero es executado por el compilador automaticamente
 - Se registra con nombre y opcionalmente su propio parseo de argumentos
 - Se compila como biblioteca dinamica y se pasa al driver de clang:

``` bash
$ clang++ -Xclang -load -Xclang miplugin.so -Xclang \
          -plugin -Xclang miplugin
```

 - Cualquier argumento **para clang, no para el driver** debe llevar `-Xclang` delante

---

### Clang API: Plugins

 - En lugar de `FrontentAction` como punto de entrada, usamos `PluginASTAction`, que permite
   implementar parseo de argumentos:

``` cpp
class MyPluginAction : public PluginASTAction<MyPluginAction> {
    bool ParseArgs(const CompilerInstance &CI,
		   const std::vector<std::string>& args) {
        ...

        return true;
    }
};
```

---

### Clang API: Plugins

 - Registramos el plugin con un nombre y su descripcion:

``` cpp
static FrontendPluginRegistry::Add<MyPluginAction> X("my-plugin", "my plugin description");
```

---

libclang
========

 - API estable entre versiones
 - Interfaz C. Bindings para otros lenguajes

---

[cppast](https://github.com/foonathan/cppast)
=============================================

 - libclang es bastante "particular"
 - Wrapper C++ moderno de libclang
 - Soluciona muchas de las particularidades de libclang
 - Activamente mantenido. Feedback rapido.

---

# }
