
# clang-tidy

## Referencias
 - https://www.kdab.com/clang-tidy-part-1-modernize-source-code-using-c11c14/

## Instalacion

 - ubuntu: `aput install clang-tidy`


## Misc

 - list checkers: `clang-tidy --list-checks -checks='*'`
 - ejecutar con `compile_commands.json`: `cd build/ && run-clang-tidy.py ...`
